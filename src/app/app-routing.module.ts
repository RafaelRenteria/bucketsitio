import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component'
import { SigninComponent } from './components/signin/signin.component';
import { BucketServiceComponent } from './components/bucket-service/bucket-service.component';
import { PaymentFeedbackComponent } from './components/payment-feedback/payment-feedback.component';
import { IndexComponent } from './components/index/index.component';
import { StripeComponent } from './components/stripe/stripe.component';
import {AutorizationService} from './services/autorization.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'bucket-service', component: BucketServiceComponent,canActivate: [AutorizationService] },
  { path: 'payment_feedback', component: PaymentFeedbackComponent,canActivate: [AutorizationService] },
  { path: 'stripe', component: StripeComponent,canActivate: [AutorizationService] },
  { path: '**', component: IndexComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
