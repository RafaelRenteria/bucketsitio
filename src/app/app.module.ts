import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { DlDateTimeDateModule, DlDateTimePickerModule } from 'angular-bootstrap-datetimepicker';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatExpansionModule} from '@angular/material/expansion';
import { AgmCoreModule } from '@agm/core';
import { Module as StripeModule } from "stripe-angular"
import {MatDialogModule} from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SigninComponent } from './components/signin/signin.component';
import { FooterComponent } from './components/footer/footer.component';
import { BucketServiceComponent,DialogOverviewExampleDialog } from './components/bucket-service/bucket-service.component';
import { GooglemapComponent } from './components/googlemap/googlemap.component';
import { PaymentFeedbackComponent } from './components/payment-feedback/payment-feedback.component';
import { IndexComponent } from './components/index/index.component';
import { StripeComponent } from './components/stripe/stripe.component';
import { PaypalComponent } from './components/paypal/paypal.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    SigninComponent,
    FooterComponent,
    BucketServiceComponent,
    GooglemapComponent,
    PaymentFeedbackComponent,
    IndexComponent,
    StripeComponent,
    DialogOverviewExampleDialog,
    PaypalComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatSelectModule,
    DlDateTimeDateModule,  // <--- Determines the data type of the model
    DlDateTimePickerModule,
    MatSlideToggleModule,
    MatExpansionModule,
    //module for google maps , token taken from my account
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCtFHkX2TLX4qDfwcVO0-3lHQV0zXkBato',
      libraries: ["places"]
    }),
    StripeModule.forRoot(),
    ReactiveFormsModule,
    MatDialogModule

  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogOverviewExampleDialog//dialog inside service to add Credit Card
  ]
})
export class AppModule { }
