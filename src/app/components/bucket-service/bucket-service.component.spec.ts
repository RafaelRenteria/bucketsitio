import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BucketServiceComponent } from './bucket-service.component';

describe('BucketServiceComponent', () => {
  let component: BucketServiceComponent;
  let fixture: ComponentFixture<BucketServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BucketServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BucketServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
