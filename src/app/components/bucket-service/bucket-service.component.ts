import { Component, OnInit, Inject } from '@angular/core';
import { GooglemapComponent } from '../googlemap/googlemap.component'
import { Router } from '@angular/router';
import { PaymentService } from 'src/app/services/payment.service'
import { MapsAPILoader } from '@agm/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface LatLng {
  latitude: number;
  longitude: number;
}
export interface TypeOfVehicle {
  value: string;
  viewValue: string;
}
export interface TypeOfService {
  value: string;
  viewValue: string;
}
export interface PayMethod {
  value: string;
}
@Component({
  selector: 'app-bucket-service',
  templateUrl: './bucket-service.component.html',
  styleUrls: ['./bucket-service.component.css']
})
export class BucketServiceComponent implements OnInit {
  currentVehicle: String;
  currentService: String;
  currentPayMethod: String;
  currentAddress: String;
  flagValidateDate: boolean = true;
  dirty: boolean = false;
  google: GooglemapComponent;
  step = 0;
  dateLabel: String;
  selectedDate: Date;
  panelOpenState = true;
  flagInvalidForm: boolean = false;
  TypeOfVehicles: TypeOfVehicle[] = [
    { value: '1', viewValue: 'Auto' },
    { value: '2', viewValue: 'Camioneta' },
    { value: '3', viewValue: 'Camión' }
  ];
  TypeOfServices: TypeOfService[] = [
    { value: '1', viewValue: 'Paquete express' },
    { value: '2', viewValue: 'Paquete basíco' },
    { value: '3', viewValue: 'Lavado VIP' },
    { value: '4', viewValue: 'Lavado premium' },
  ];
  PayMethods: PayMethod[] = [
    { value: 'Efectivo' },
    { value: 'Tarjeta' },
  ];

  constructor(public dialog: MatDialog, private router: Router, private paymentService: PaymentService, private mapsAPILoader: MapsAPILoader) {
    let validatedDate = new Date();
    this.dateLabel = new Date(validatedDate.getTime() + 3600000).toLocaleString();
    this.selectedDate = new Date();
    this.flagValidateDate = true;


  }

  ngOnInit() {
    this.flagValidateDate = true;
  }
  ngAfterViewInit() {
    this.flagValidateDate = true;
  }
  onSubmit() {
    //validate date
    this.flagValidateDate = this.validateDate();
    if (this.currentPayMethod != undefined && this.currentService != undefined && this.currentVehicle != undefined && this.flagValidateDate) {
      this.flagInvalidForm = false;
      //the components GoogleMap compute the areaselected between areas and stored in the static variable areaSelected
      // console.log('Area = ' + GooglemapComponent.areaSelected);
      console.log(this.currentVehicle, this.currentService, this.currentPayMethod, this.dirty);
      //set and share data to service then to component
      this.paymentService.setselectedVehicle(this.currentVehicle);
      this.paymentService.setdirty(this.dirty == true ? 1 : 0);
      let serviceDescription;
      switch (this.currentService) {
        case "1":
          serviceDescription =
            "Paquete express: Lavado\nCristales\nAbrillantador de\nllantas y moldduras\n(30min) 120 pesos";
          break;
        case "2":
          serviceDescription =
            "Paquete básico: Lavado\nCristales\nAbrillantador de\nllantas y moldduras\nAspirado\n(40min) 130 pesos";
          break;
        case "3":
          serviceDescription =
            "Lavado VIP: Lavado\nCristales\nAbrillantador de\nllantas y moldduras\nAspirado\nDetalle de interiores\n(1 hora) 150 pesos";
          break;
        case "4":
          serviceDescription =
            "Lavado Premium: Lavado\nCristales\nAbrillantador de\nllantas y moldduras\nAspirado\nDetalle de interiores\nEncerado\n(2 hora) 220 pesos";
          break;
        default:
          serviceDescription =
            "Paquete default: Lavado\nCristales\nAbrillantador de\nllantas y moldduras\n(30min) 120 pesos";
          break;
      }
      this.paymentService.setselectedService(serviceDescription);
      this.paymentService.setselectedPaymentMethod(this.currentPayMethod);
      //date
      this.paymentService.setserviceDate(this.dateLabel);
      //GooglemapComponent Variables are retrived from googlemapComponent
      this.paymentService.setselectedArea(GooglemapComponent.areaSelected.toString());

      //This function gets the current addres and once its done navigate to pay feed back component
      this.getCurrentAddressAndNavigate(GooglemapComponent.currentUbication, this.router, this.paymentService);

    }
    else {
      this.flagInvalidForm = true;
    }

  }

  //This function should be implemented as async and await or promises instead of this hot fix
  getCurrentAddressAndNavigate(currentLocation: LatLng, _router: Router, _paymentService: PaymentService) {
    this.mapsAPILoader.load().then(() => {
      let geocoder = new google.maps.Geocoder;
      let latlng = { lat: currentLocation.latitude, lng: currentLocation.longitude };
      geocoder.geocode({ 'location': latlng }, function (results) {
        if (results[0]) {

          this.currentAddress = results[0].formatted_address;
          console.log(this.currentAddress);
          //set current addres to service to share with payment-feedback component
          _paymentService.setaproxVehicleUbication(this.currentAddress)
          _router.navigate(['payment_feedback']);
        }
      });
    });
  }

  validateDate(): boolean {

    let currentDate = new Date;
    console.log(this.selectedDate.getTime(), currentDate.getTime());
    //current time plus 1 hour
    if (this.selectedDate.getTime() > currentDate.getTime() + 3600000) {
      return true;
    } else
      return false;


  }
  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
  onCustomDateChange($event) {
    if ($event != undefined) {
      this.dateLabel = this.selectedDate.toLocaleString();
      this.nextStep();
      this.step = 0;
    }
  }
  addCard() {
    if (this.currentPayMethod.toString() == 'Agregar tarjeta') {
      this.openDialog();
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '400px',
      height: '130px',
    
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

    });
  }

}//end class

//Dialog credit card

const templateDialog = `
  <app-paypal></app-paypal>
`;

@Component({
  selector: 'dialog-overview-example-dialog',
  template: templateDialog,
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>) { }
  //dismiss modal
  public onNoClick(): void {
    this.dialogRef.close();
  }

}



