import { Component, OnInit, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Observable } from 'rxjs'
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { FormControl } from '@angular/forms';


export interface LatLng {
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-googlemap',
  templateUrl: './googlemap.component.html',
  styleUrls: ['./googlemap.component.css']
})

export class GooglemapComponent implements OnInit {

  @ViewChild("search", { static: false })
  public searchElementRef: ElementRef;
  address: string;
  private geoCoder;
  //this variables are static to share with the service component
  public searchControl: FormControl;
  static currentUbication: LatLng = { latitude: 0, longitude: 0 };
  static areaSelected: Number = 0;
  //Tequis
  initialUbication: LatLng = { latitude: 22.1507137, longitude: -100.992567 };
  //Initialize math library
  math = Math;
  meters: Number;
  //Variables for define Circle Areas in google map
  firsCircleArearadius: number = 3000;
  seccondCircleArearadius: number = this.firsCircleArearadius + 6000;
  thirdCircleArearadius: number = this.seccondCircleArearadius + 15000;
  latMap: number = 22.1507137;
  lngMap: number = -100.992567;
  latMarker: number = this.latMap;
  lngMarker: number = this.lngMap;

  coordinates: any;

  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) {

  }

  ngOnInit() {
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latMap = place.geometry.location.lat();
          this.lngMap = place.geometry.location.lng();

        });
      });
    });

  }

  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latMap = $event.coords.lat;
    this.lngMap = $event.coords.lng;
    this.getAddress(this.latMap, this.lngMap);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latMap = position.coords.latitude;
        this.lngMap = position.coords.longitude;
        this.getAddress(this.latMap, this.lngMap);
      });
    }
  }

  //Updates the marker position from onCameraMoves
  placeMarker($event) {
    //Updates the marker google map position
    this.latMarker = $event.lat;
    this.lngMarker = $event.lng;
    //update the component variable to share with the service component
    GooglemapComponent.currentUbication.latitude = $event.lat;
    GooglemapComponent.currentUbication.longitude = $event.lng;

    // console.log($event.lat, $event.lng);
    //GET AREA
    this.meters = this.getDistanceBetweemAreaAndMarker(GooglemapComponent.currentUbication, this.initialUbication);
    if (this.meters < this.firsCircleArearadius) {
      GooglemapComponent.areaSelected = 1;
    } else if (this.meters > this.firsCircleArearadius &&
      this.meters < this.seccondCircleArearadius) {
      GooglemapComponent.areaSelected = 2;
    } else if (this.meters > this.seccondCircleArearadius &&
      (this.meters < this.thirdCircleArearadius)) {
      GooglemapComponent.areaSelected = 3;
    } else if (this.meters > this.thirdCircleArearadius) {
      GooglemapComponent.areaSelected = -1;
    }

  }
  public getPosition(): Observable<Position> {
    return Observable.create(
      (observer) => {
        navigator.geolocation.watchPosition((pos: Position) => {
          observer.next(pos);
          console.log(pos.coords.latitude, pos.coords.longitude);

        }),
          () => {
            console.log('Position is not available');
          },
          {
            enableHighAccuracy: true
          };
      });
  }
  public ded2rad(x): number {
    return x * this.math.PI / 180;

  }


  getDistanceBetweemAreaAndMarker(p1: LatLng, p2: LatLng): Number {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = this.ded2rad(p2.latitude - p1.latitude);
    var dLong = this.ded2rad(p2.longitude - p1.longitude);
    var a = this.math.sin(dLat / 2) * this.math.sin(dLat / 2) +
      this.math.cos(this.ded2rad(p1.latitude)) *
      this.math.cos(this.ded2rad(p2.latitude)) *
      this.math.sin(dLong / 2) *
      this.math.sin(dLong / 2);
    var c = 2 * this.math.atan2(this.math.sqrt(a), this.math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
  }
}
