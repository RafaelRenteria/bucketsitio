import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorsFromServer: String;

  constructor(private userService: UsersService,private router:Router) {
    this.errorsFromServer = '';
  }

  ngOnInit() {
  }


  cleanForm(form?: NgForm) {
    if (form) {
      form.reset();
    }
  }
  onSubmit(form: NgForm) {
    console.log(form.value);
    if (form.value.email !== undefined && form.value.password !== undefined) {
      this.userService.loginUsuario(form.value).subscribe((res) => {
        console.log('Response :', res);
        console.log('Data ', res['data']);
        if (res['status'] == 'success') {
          this.errorsFromServer='';
          let codedToken = res['data'].api_token;
          //store in local storage
          this.userService.setToken(codedToken);
          this.userService.setDate(Date.now());
          console.log('Token guardada :', this.userService.getToken())
          this.router.navigateByUrl('/bucket-service');

        }else{
          this.errorsFromServer = res['message'];
        }
      }, error => {
        console.log(error);
        this.errorsFromServer = error;
      });
    }


  }

}
