import { Component, OnInit } from '@angular/core';
import{UsersService} from 'src/app/services/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private userService:UsersService,private router:Router) { }

  ngOnInit() {

  }
  index(){
    this.router.navigateByUrl('/');
  }

  logIn(){
    this.router.navigateByUrl('/login');
  }

  service(){
    this.router.navigateByUrl('/bucket-service');
  }

  signIn(){
    this.router.navigateByUrl('/signin');
  }


  logOut(){
    this.userService.removeLocalStorage();
    this.router.navigateByUrl('/');
  }

}
