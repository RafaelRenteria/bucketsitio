import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentFeedbackComponent } from './payment-feedback.component';

describe('PaymentFeedbackComponent', () => {
  let component: PaymentFeedbackComponent;
  let fixture: ComponentFixture<PaymentFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
