import { Component, OnInit, Input, NgZone } from '@angular/core';
import { PaymentService } from 'src/app/services/payment.service';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service'
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-payment-feedback',
  templateUrl: './payment-feedback.component.html',
  styleUrls: ['./payment-feedback.component.css']
})
export class PaymentFeedbackComponent implements OnInit {

  amount: String;
  vehicle: String;
  service: String;
  paymentMethod: String;
  address: String;
  schedule: String;
  area: String;
  constructor(private paymentService: PaymentService, 
    private ngZone: NgZone,
    private router: Router,
    ) {
    this.vehicle = this.paymentService.getselectedVehicle();
    this.service = this.paymentService.getselectedService();
    this.paymentMethod = this.paymentService.getselectedPaymentMethod();
    this.address = this.paymentService.getaproxVehicleUbication();
    this.area = this.paymentService.getselectedArea();
    this.schedule = this.paymentService.getserviceDate();
    this.amount = '1.00';
    //pass the amount to the service so we can share it with the paypal component
    this.paymentService.setchargeService(this.amount);
  }

  ngOnInit() {
  }

  onCancel() {
    this.ngZone.run(() => {
      this.router.navigate(['/']);

    })

  }
}
