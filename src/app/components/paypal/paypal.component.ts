import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import{PaymentService} from 'src/app/services/payment.service';

declare var paypal;

@Component({
  selector: 'app-paypal',
  templateUrl: './paypal.component.html',
  styleUrls: ['./paypal.component.css']
})
export class PaypalComponent implements OnInit {
  constructor(private paymentService:PaymentService){

  }

  @ViewChild('paypal', { static: true }) paypalElement: ElementRef;


  paidFor = false;

  ngOnInit() {
    paypal
      .Buttons({
        style: {
          color: 'blue',
          shape: 'pill',
          label: 'pay',
          size: 'responsive'

        },
        createOrder: (data, actions) => {
          return actions.order.create({
            purchase_units: [
              {
                description: this.paymentService.getselectedService(),
                amount: {
                  currency_code: 'MXN',
                  value: this.paymentService.getchargeService()
                }
              }
            ]
          });
        },
        onApprove: async (data, actions) => {
          const order = await actions.order.capture();
          this.paidFor = true;
          console.log(order);
        },
        onError: err => {
          console.log(err);
        }
      })
      .render(this.paypalElement.nativeElement);
  }
  
}
