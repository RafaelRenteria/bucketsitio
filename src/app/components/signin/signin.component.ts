import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, NgForm } from '@angular/forms';
import { CustomValidators } from '../../form-validators/custom-validators';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user'
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  hide = true;
  passwordsMatch = false;
  messageFromServer:String;
  constructor(private userService: UsersService, private router: Router) { 
    this.messageFromServer='';
  }

  ngOnInit() {
  }

  confirmPassword = new FormControl('', Validators.required);
  password = new FormControl('', [Validators.compose([Validators.required,
  // 2. check whether the entered password has a number
  CustomValidators.patternValidator(/\d/, { hasNumber: true }),
  // 3. check whether the entered password has upper case letter
  CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
  // 4. check whether the entered password has a lower-case letter
  CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
  Validators.minLength(8),])]);

  getErrorMessage() {
    return this.password.hasError('required') ? 'Campo Obligatorio' :
      this.password.hasError('hasCapitalCase') ? 'Minimo una MAYUSCULA' :
        this.password.hasError('hasSmallCase') ? 'Minimo una MINUSCULA' :
          this.password.hasError('hasNumber') ? 'Minimo un NUMERO' :
            this.password.hasError('minlength') ? 'Minimo 8 CARACTERES' :
              '';
  }
  matchPasswords(): void {
    if (this.confirmPassword.value !== this.password.value)
      this.passwordsMatch = true;
    else
      this.passwordsMatch = false;

  }

  cleanForm(form?: NgForm) {
    if (form) {
      form.reset();
    }
  }
  onSubmit(form: NgForm) {
    console.log(form.value);
    this.matchPasswords();
    if (!this.passwordsMatch && form.value.name !== undefined && form.value.email !== undefined) {
      console.log('Formulario correcto');
      let userModel: User;
      userModel = new User;
      userModel.email = form.value.email;
      userModel.name = form.value.name;
      userModel.password = this.password.value;
      userModel.phone = form.value.phone;
      this.userService.signInUsuario(userModel).subscribe(res => {
        if (res['status'] == 'success') {
          console.log(res);
          this.messageFromServer=res['message'];
          setTimeout(() => {
            this.router.navigateByUrl("/login");
          },
            4000);
          

        }
      }, error => {
        console.log(error);

      });

    }
  }


}
