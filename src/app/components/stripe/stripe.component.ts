import { Component, OnInit } from '@angular/core';
import { StripeScriptTag, StripeToken } from "stripe-angular"
import { StripeSource } from 'stripe-angular/StripeTypes';
import { DialogOverviewExampleDialog } from '../bucket-service/bucket-service.component';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.css']
})
export class StripeComponent implements OnInit {
  private publishableKey: string = "pk_test_EeolYG9gXj7xRQGzS4isDDWp00bk9Yls1E";
  constructor(private userService: UsersService, public StripeScriptTag: StripeScriptTag, private dialog: DialogOverviewExampleDialog) {
    this.StripeScriptTag.setPublishableKey(this.publishableKey)
  }
  token: string;
  extraData = {
    "name": String,
    "address_city": String,
    "address_line1": String,
    "address_line2": String,
    "address_state": String,
    "address_zip": String
  }

  onStripeInvalid(error: Error) {
    console.log('Validation Error', error)
  }

  setStripeToken(token: StripeToken) {
    console.log('Stripe token', token)
    this.dismissDialog();
    this.userService.addCard(token.id, token.card.last4).subscribe((res) => {
      console.log('Response :', res);
      console.log('Data ', res['data']);
      if (res['status'] == 'success') {
        //saves in local storage
        this.userService.setStripeToken(token.id);        
      }

    });
  }

  setStripeSource(source: StripeSource) {
    console.log('Stripe source', source)
  }

  onStripeError(error: Error) {
    console.error('Stripe error', error)
  }
  //call the function form service component to dismiis the modal
  public dismissDialog(): void {
    this.dialog.onNoClick();
  }
  ngOnInit() {
  }

}
