export class User{
    idUser:String;
    email:String;
    name:String;
    password:String;
    phone:String;

    
    constructor(idUser='',name='', email = '', password = '',phone=''){
        this.idUser =idUser;
        this.name=name;
        this.email = email;
        this.password = password; 
        this.phone=phone;
    }
}