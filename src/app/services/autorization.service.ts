import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import  {UsersService} from './users.service';

@Injectable({
  providedIn: 'root'
})
export class AutorizationService {

  constructor(private userService:UsersService,private router:Router) { }
  canActivate(): boolean {
    if (this.userService.getToken()===null)  {
	  this.router.navigate(['/']);
	  return false;
    }
    return true;
  }
}
