import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  private selectedVehicle: String;
  private selectedService: String;
  private selectedPaymentMethod: String;
  private aproxVehicleUbication: String;
  private serviceDate: String;
  private selectedArea: String;
  private dirty: Number;
  private chargeService: String;

  constructor() {
    this.selectedVehicle = null;
    this.selectedPaymentMethod = null;
    this.selectedService = null;
    this.aproxVehicleUbication = null;
    this.serviceDate = null;
    this.selectedArea = null;
    this.dirty = null;
    this.chargeService = null;
  }

  getselectedVehicle(): String {
    return this.selectedVehicle;
  }
  setselectedVehicle(vehicle: String) {
    this.selectedVehicle = vehicle;
  }
  getselectedService(): String {
    return this.selectedService;
  }
  setselectedService(vehicle: String) {
    this.selectedService = vehicle;
  }
  getselectedPaymentMethod(): String {
    return this.selectedPaymentMethod;
  }
  setselectedPaymentMethod(vehicle: String) {
    this.selectedPaymentMethod = vehicle;
  }
  getaproxVehicleUbication(): String {
    return this.aproxVehicleUbication;
  }
  setaproxVehicleUbication(vehicle: String) {
    this.aproxVehicleUbication = vehicle;
  }

  getserviceDate(): String {
    return this.serviceDate;
  }
  setserviceDate(vehicle: String) {
    this.serviceDate = vehicle;
  }
  getselectedArea(): String {
    return this.selectedArea;
  }
  setselectedArea(vehicle: String) {
    this.selectedArea = vehicle;
  }
  getdirty(): Number {
    return this.dirty;
  }
  setdirty(vehicle: Number) {
    this.dirty = vehicle;
  }
  
  setchargeService(_chargeService: String): void {
    this.chargeService = _chargeService;
  }
  getchargeService() {
    return this.chargeService;
  }
}
