import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http'
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import * as myGlobals from '../globals';
import { User } from '../models/user'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private token: string;
  private date: number;
  private stripeToken: string;

  constructor(private http: HttpClient, private router: Router) {
    this.token = null;
    this.date = null;
    this.stripeToken = null;

  }

  addCard(_stripeToken: string, _last4: string) {
    //CORS Requirements
    let myheaders = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
      'Content-Type': 'application/json'
    });
    if (this.token === undefined)
      this.getToken();

    return this.http.post(myGlobals.apiUrl + 'client/addcard', [
      { "api_token": this.token },
      { "card_id": _stripeToken }
    ], {
        headers: myheaders
      });

  }

  loginUsuario(user: User): Observable<any> {
    //CORS Requirements
    let myheaders = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
      'Content-Type': 'application/json'
    });

    return this.http.post(myGlobals.apiUrl + 'login', user, {
      headers: myheaders
    });

  }
  signInUsuario(user: User): Observable<any> {
    //CORS Requirements
    let myheaders = new HttpHeaders({
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PATCH, PUT, DELETE, OPTIONS",
      "Access-Control-Allow-Headers": "Origin, Content-Type, X-Auth-Token",
      'Content-Type': 'application/json'
    });

    return this.http.post(myGlobals.apiUrl + 'client/register', user, {
      headers: myheaders
    });

  }

  //set stripeToken
  setStripeToken(_stripeToken: string) {
    if (this.token !== undefined) {
      this.stripeToken = _stripeToken;
      window.localStorage.setItem('stripeToken', this.stripeToken);
    }

  }
  // get stripeToken
  getStripeToken() {
    if (this.token !== undefined) {
      this.stripeToken = window.localStorage.getItem('stripeToken');
      console.log('StripeToken from Local storage ' + this.stripeToken);

    }
    return this.stripeToken;
  }

 

  //set ang get date
  setDate(_date: number) {
    if (this.token !== undefined) {

      this.date = _date;
      window.localStorage.setItem('date', this.date.toString());
    }

  }
  getDate() {
    if (this.token !== undefined) {

      this.date = Number.parseInt(window.localStorage.getItem('date'));
      //console.log('Date de inicio de secion : ', this.date)
      //console.log('Date actual : ', Date.now())
    }
    return this.date;
  }

  //saves in local storage
  setToken(_token: string) {
    this.token = _token;
    window.localStorage.setItem('token', this.token);

  }
  //gets from local storage
  getToken(): string {

    if (this.token === null) {
      this.token = window.localStorage.getItem('token');
      console.log('Token recarcado de local starage: ', this.token)

    } else {
      //check expiration date 
      if (Date.now() > this.getDate() + 3600000) {
        //remove token and date
        this.removeLocalStorage();
      }
    }
    return this.token;
  }

  //para logout hay q removeItem y hay que poner el token en null
  removeLocalStorage() {
    this.token = null;
    this.date = null;
    window.localStorage.removeItem('token');
    window.localStorage.removeItem('date');
    location.replace('/');
  }
}
